FROM python:3.11-slim
WORKDIR /usr/src/app
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
COPY src/app.py .
COPY src/Engine.py .
COPY src/d2v_v2.model .
COPY src/cs425-news-database-firebase-adminsdk-rter4-fbdf75f1ef.json .
CMD ["python", "./app.py"]
